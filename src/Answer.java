import java.sql.SQLOutput;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;

public class Answer {

   public static void main (String[] param) throws InterruptedException {

      // TODO!!! Solutions to small problems
      // that do not need an independent method!
    
      // conversion double -> String
      double d=1.1;
      System.out.println("double -> "+d);
      String convDtoS = String.valueOf(d);
      System.out.println("-> string "+convDtoS);

      // conversion String -> int
      String convStoI = "123";
      System.out.println("string -> "+convStoI);
      int num = Integer.parseInt(convStoI);
      System.out.println("-> int "+num);

      // "hh:mm:ss"
      System.out.println("Time in hh:mm:ss -> "+java.time.LocalTime.now());

      // cos 45 deg
      double cos = 45.0;
      System.out.println("cos45 -> "+Math.cos(cos));

      // table of square roots
      for(int i=0;i<=100;i++){
         //System.out.println("Square"+i+": "+Math.sqrt(i));
      }

      // Uppercase <-> Lowercase
      String firstString = "ABcd12";
      String result = reverseCase (firstString);
      System.out.println ("\"" + firstString + "\" -> \"" + result + "\"");

      // reverse order
      String str = "Hello";
      System.out.println(str+" --> "+ new StringBuilder(str).reverse().toString());

      // count words
      String s = " Can  you count   ";
      int nw = countWords (s);
      System.out.println (s + "\t" + String.valueOf (nw));

      // measure time sec
      // pause. COMMENT IT OUT BEFORE JUNIT-TESTING!
      long startMillis = System.currentTimeMillis();
      TimeUnit.SECONDS.sleep(3);
      long endMillis = System.currentTimeMillis();
      System.out.println(endMillis-startMillis+" ms");

      // generate random array<Integer>
      final int LSIZE = 100;
      ArrayList<Integer> randList = new ArrayList<Integer> (LSIZE);
      Random generaator = new Random();
      for (int i=0; i<LSIZE; i++) {
         randList.add (Integer.valueOf (generaator.nextInt(1000)));
      }
      System.out.println("randList:"+randList);
      /*int is a type, Integer is a class
      ex) string x = "10"
      int x = int(x) notworking
      int x = Integer.perseInt(x) is working
      */

      // minimal element
      int mini=randList.get(0);
      for(int i=1;i<randList.size();i++){
         if(mini > randList.get(i)){
            mini = randList.get(i);
         }
      }
      //System.out.println("minimal :"+mini);

      // HashMap tasks:
      //    create
      //    print all keys
      //    remove a key
      //    print all pairs
      Map <String,String> subjects = new HashMap<>();
      subjects.put("ics0005","Algorithm and Data");
      subjects.put("ics0003","Introduction to Cybersecurity");
      subjects.put("ics0015","Fundamental of Python");
      System.out.println("Before: "+subjects);
      subjects.remove("ics0015");
      System.out.println("After: "+subjects);

      System.out.println ("Before reverse:  " + randList);
      reverseList (randList);
      System.out.println ("After reverse: " + randList);

      System.out.println ("Maximum: " + maximum (randList));
   }

   /** Finding the maximal element.
    * @param a Collection of Comparable elements
    * @return maximal element.
    * @throws NoSuchElementException if <code> a </code> is empty.
    */
   static public <T extends Object & Comparable<? super T>>
         T maximum (Collection<? extends T> a) 
            throws NoSuchElementException {
      // TODO!!! Your code here
      return Collections.max(a);
   }

   /** Counting the number of words. Any number of any kind of
    * whitespace symbols between words is allowed.
    * @param text text
    * @return number of words in the text
    */
   public static int countWords (String text) {
      String [] words = text.split("\\s+");
      List <String> list = new ArrayList<String>(Arrays.asList(words));
      //System.out.println("OLD LIST:"+list);
      for(int i=0;i<list.size();i++){
         if(list.get(i).isEmpty()){
            list.remove(i);
         }
      }
      //System.out.println("NEW LIST :"+list);
      return list.size(); // TODO!!! Your code here
   }

   /** Case-reverse. Upper -> lower AND lower -> upper.
    * @param s string
    * @return processed string
    */
   public static String reverseCase (String s) {
      char [] tmp = s.toCharArray();
      for(int j=0;j<s.length();j++){
         if(Character.isLowerCase(tmp[j])){
            tmp[j]=Character.toUpperCase(tmp[j]);
         }
         else if(Character.isUpperCase(tmp[j])){
            tmp[j]=Character.toLowerCase(tmp[j]);
         }
         else{
            tmp[j]=tmp[j];
         }
      }
      s = new String(tmp);
      //System.out.println(s);
      return s; // TODO!!! Your code here
   }

   /** List reverse. Do not create a new list.
    * @param list list to reverse
    */
   public static <T extends Object> void reverseList (List<T> list)
      throws UnsupportedOperationException {
         // TODO!!! Your code here
         Collections.reverse(list);
   }
}
